
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите ширину (нечётное число): ");
        String s = scanner.next();
        if (Integer.parseInt(s) % 2 == 0) {
            System.out.println("Число чётное");
        } else {
            paint(s);
        }
    }

    private static void paint(String s) {
        boolean turn = true;                      //Переменная для поворота треугольников часов
        int size = Integer.parseInt(s);
        ArrayList<String> stringArrayList = new ArrayList<>();      //Создание массива который будет отвечать за вывод часов
        for (int i = 0; i < size; i++) {
            stringArrayList.add("*");                        //заполнение этого массива
        }
        int forAdd = size / 2;          // переменная центра массива для удобства

        for (int i = 0; i < size; i++) {                //цикл вывода часов
            if (turn) {
                if (i + 1 != size) {
                    System.out.println(stringArrayList);
                    if (!stringArrayList.get(forAdd - 1).equals(" ")) {
                        stringArrayList.set(i, " ");
                        stringArrayList.set(stringArrayList.size() - 1 - i, " ");
                    } else {
                        turn = false;
                        i = 0;             //устанавливая счётчик обратно на ноль цикл начнёт свою работу начиная с 1
                                           // таким образом я могу себе позволить не переживать за центральную точку
                    }                      // а использовать её как опорную.
                }
            } else {
                if (i + 1 != size) {
                    if (!stringArrayList.get(0).equals("*")) {
                        stringArrayList.set(forAdd - i, "*");
                        stringArrayList.set(forAdd + i, "*");
                        System.out.println(stringArrayList);
                    } else {
                        break;
                    }
                }
            }
        }
    }
}
